import random
import matplotlib.pyplot as mpl
import cv2

img = cv2.imread("tiger.png") 
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

for x in range(0, len(hsv)):
    for y in range(0, len(hsv[0])):
        hsv[x][y][2] += 50

img = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
cv2.imwrite("tiger2.png", img)
