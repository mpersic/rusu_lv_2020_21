import random
import matplotlib.pyplot as mpl

file = open("mtcars.csv")
data = file.readlines()
file.close()

xs = []
ys = []
wt = []

del(data[0])

for i in range(len(data)):
    d = data[i].split(",")
    mpg = float(d[1].strip())
    hp = float(d[4].strip())
    wet = float(d[6].strip())

    xs.append(mpg)
    ys.append(hp)
    wt.append(wet)

minmpg = 1000000
maxmpg = -1000000
summpg = 0
for i in range(len(xs)):
    minmpg = min(minmpg, xs[i])
    maxmpg = max(maxmpg, xs[i])
    summpg += xs[i]

print(minmpg, maxmpg, summpg / len(xs))

mpl.scatter(xs,ys, color = "r")
mpl.scatter(xs,wt, color = "b")
mpl.show()
