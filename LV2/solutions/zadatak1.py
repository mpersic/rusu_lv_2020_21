# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 16:32:11 2021

@author: Matej
"""
import re

f = open("mbox-short.txt", "r")
text = f.read()

regex = r'([a-zA-Z0-9+._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)'

emails = re.findall(regex, text)
emails = "\n".join(emails)
emails = re.sub('@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+', '', emails)

print(emails)