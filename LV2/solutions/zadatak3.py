# -*- coding: utf-8 -*-
"""
Napravite  jedan  vektor  proizvoljne  dužine  koji  sadrži  slučajno  generirane  cjelobrojne  vrijednosti  0  ili  1.  Neka  1 
označava mušku osobu, a 0 žensku osobu. 
Napravite drugi vektor koji sadrži visine osoba koje se dobiju uzorkovanjem 
odgovarajuće  distribucije.  U  slučaju  muških  osoba  neka  je  
to  Gaussova  distribucija  sa  srednjom  vrijednošću  180  cm  i 
standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to 
Gaussova distribucija sa srednjom vrijednošću 167 cm 
i  standardnom  devijacijom  7  cm.  Prikažite  podatke  te  ih  
obojite  plavom  (1)  odnosno  crvenom  bojom  (0).  Napišite 
funkciju  koja  računa  srednju  vrijednost  visine  za  muškarce  
odnosno  žene  (probajte  izbjeći  for  petlju  pomoću  funkcije 
np.dot). Prikažite i dobivene srednje vrijednosti na grafu. 
"""
#[]
import numpy as np
import matplotlib.pyplot as plt


def getAvg(nums):

    s = 0
    for i in range(len(nums)):
        s += nums[i]

    return s / len(nums)

people = []
for index in range(100):
    people.append(np.random.randint(0,2))
    
    
womanHeight = []
manHeight = []
for index in range(100):
    if people[index] == 0:
        womanHeight.append(np.random.normal(167,7,None))
    else:
        manHeight.append(np.random.normal(180,7,None))
        
mavg = getAvg(manHeight)
favg = getAvg(womanHeight)
    
plt.plot(manHeight, color="b")
plt.axline((0, mavg), (len(manHeight), mavg), color="b")
plt.plot(womanHeight, color="r")
plt.axline((0, favg), (len(womanHeight), favg), color="r")
plt.show()