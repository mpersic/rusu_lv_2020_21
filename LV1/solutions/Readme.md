Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci.
Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). 
Međutim, skripta ima bugove i ne radi kako je zamišljeno.

Ispravio sam potrebne bugove i skripta radi kako je i zamišljeno. Bilo je problema s print funkcijom, nedostajale su zagrade.
Funkcija raw_input promjenjena je u input. Zadnja promjena je ispravak brojača na kraju koda.