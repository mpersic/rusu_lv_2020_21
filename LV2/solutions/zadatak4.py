# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 18:02:07 2021

@author: Matej
"""

import numpy as np
import matplotlib.pyplot as plt

dice = []
for index in range(100):
    dice.append(np.random.randint(0,6))
    
plt.hist(dice)